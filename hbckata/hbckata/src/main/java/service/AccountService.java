package service;

import common.OperationEnum;
import common.exception.KataException;
import model.Account;
import model.BankingOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import repository.AccountRepository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Optional;


@Component
public  class AccountService  {

    AccountRepository accountRepository;

    @Autowired
    public AccountService(AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    public void operation(Long id , BigDecimal amount, OperationEnum operation) throws KataException{
        Optional<Account> accountOptional = accountRepository.findById(id);
       if(accountOptional.isPresent()){
           Account account = accountOptional.get();
           BankingOperation bankingOperation = computeBankingOperation(amount, operation, account);
           operation.exec(account,bankingOperation);
        }else{
         throw new KataException("cet id n'esxite pas {}"+id);
       }

    }


    public List<BankingOperation> getOperations(Long id ) throws KataException{
        Optional<Account> accountOptional = accountRepository.findById(id);
        if(accountOptional.isPresent()){
            return  accountOptional.get().getBankingOperations();
        }else{
            throw new KataException("cet id n'esxite pas {}"+id);
        }

    }


    private BankingOperation computeBankingOperation(BigDecimal amount, OperationEnum operation, Account account) {
        BankingOperation.BankingOperationBuilder bankingOperationBuilder = new BankingOperation.BankingOperationBuilder();
        bankingOperationBuilder.withAmount(amount).withBalance(account.getBalance()).withOperation(operation).withDate(new Date());
        return bankingOperationBuilder.build();
    }
}
