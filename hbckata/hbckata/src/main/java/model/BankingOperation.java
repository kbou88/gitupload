package model;

import common.OperationEnum;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.util.Date;

@Entity
public class BankingOperation {

    @Id
    private Long idBankingOperation ;
    @Column
    private Date date;
    @Column
    private BigDecimal amount;
    @Column
    private BigDecimal balance;
    @Column
    @Enumerated
    private OperationEnum operation;

    private BankingOperation(Date date, BigDecimal amount, BigDecimal balance,OperationEnum operation){
        this.date=date;
        this.amount=amount;
        this.balance=balance;
        this.operation=operation;
    }

    public Long getIdBankingOperation() {
        return idBankingOperation;
    }

    public OperationEnum getOperation() {
        return operation;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getAmount() {
        return amount;
    }


    public BigDecimal getBalance() {
        return balance;
    }
    public static  class BankingOperationBuilder {

        private Date date;
        private BigDecimal amount;
        private BigDecimal balance;
        private OperationEnum operation;

        public BankingOperationBuilder withDate(Date date){
            this.date=date;
            return this;
        }

        public BankingOperationBuilder withAmount(BigDecimal amount){
            this.amount=amount;
            return this;

        }

        public BankingOperationBuilder withBalance(BigDecimal balance){
            this.balance=balance;
            return this;

        }


        public BankingOperationBuilder withOperation(OperationEnum operation){
            this.operation = this.operation;
            return this;

        }


        public BankingOperation build() {
            return new BankingOperation(this.date,this.amount,this.balance,this.operation);
        }

    }

}


