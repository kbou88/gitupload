package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
public class Account {

    @Id
    private Long idAccount;
    @Column
    private BigDecimal balance = new BigDecimal(0);
    @Column
    private Date date;

    @OneToMany
    private List<BankingOperation> bankingOperations ;

    public Long getIdAccount() {
        return idAccount;
    }

    public Date getDate() {
        return date;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public List<BankingOperation> getBankingOperations() {
        return bankingOperations;
    }

    public synchronized void withDrawal(BankingOperation bankingOperation) {
        balance= balance.add(bankingOperation.getAmount().negate());
        bankingOperations.add(bankingOperation);
    }

    public synchronized void deposit(BankingOperation bankingOperation) {
        balance= balance.add(bankingOperation.getAmount());
        bankingOperations.add(bankingOperation);
    }

}
