package api.mapper;


import api.dto.BankingOperationDto;
import model.BankingOperation;
import org.springframework.stereotype.Component;

@Component
public class BankingMapper {

    public BankingOperationDto toDto(BankingOperation bankingOperation){
        BankingOperationDto bankingOperationDto = new BankingOperationDto();
        bankingOperationDto.setAmount(bankingOperation.getAmount());
        bankingOperationDto.setBalance(bankingOperation.getBalance());
        bankingOperationDto.setDate(bankingOperation.getDate());
        bankingOperationDto.setOperation(bankingOperation.getOperation());
        bankingOperationDto.setIdBankingOperation(bankingOperation.getIdBankingOperation());
        return bankingOperationDto;
    }
}
