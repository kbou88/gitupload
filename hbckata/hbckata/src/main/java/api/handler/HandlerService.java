package api.handler;

import api.dto.BankingOperationDto;
import api.dto.Dto;
import api.mapper.BankingMapper;
import common.exception.KataException;
import model.BankingOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import service.AccountService;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class HandlerService {
    AccountService accountService;
    BankingMapper bankingMapper;


    @Autowired
    public HandlerService(AccountService accountService,BankingMapper bankingMapper){
        this.accountService = accountService;
        this.bankingMapper= bankingMapper;
    }

    public ResponseEntity operation(Long id, Dto dto){
        try {
            accountService.operation(id, dto.getAmount(), dto.getOperation());
            return new ResponseEntity(HttpStatus.OK);
        }catch (KataException e){
            // on peut faire le mapping entre exception fonctionnel ou technique et status http
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    public ResponseEntity getOperations(Long id){
        try {
            List<BankingOperation> bankingOperations= accountService.getOperations(id);
            List<BankingOperationDto> bankingOperationDtos = getBankingOperationDtos(bankingOperations);
            return new ResponseEntity(bankingOperationDtos,HttpStatus.OK);
        }catch (KataException e){
            // on peut faire le mapping entre exception fonctionnel ou technique et status http
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }

    private List<BankingOperationDto> getBankingOperationDtos(List<BankingOperation> bankingOperations) {
        return bankingOperations.stream().map((bo)->bankingMapper.toDto(bo)).collect(Collectors.toList());
    }
}
