package api.dto;

import common.OperationEnum;

import java.math.BigDecimal;

public class Dto {

    private BigDecimal amount;

    private OperationEnum operation;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public OperationEnum getOperation() {
        return operation;
    }

    public void setOperation(OperationEnum operation) {
        this.operation = operation;
    }
}


