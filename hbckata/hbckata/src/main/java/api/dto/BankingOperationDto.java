package api.dto;

import common.OperationEnum;

import java.math.BigDecimal;
import java.util.Date;

public class BankingOperationDto {


    private Long idBankingOperation ;

    private Date date;

    private BigDecimal amount;

    private BigDecimal balance;

    private OperationEnum operation;

    public Long getIdBankingOperation() {
        return idBankingOperation;
    }

    public void setIdBankingOperation(Long idBankingOperation) {
        this.idBankingOperation = idBankingOperation;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public OperationEnum getOperation() {
        return operation;
    }

    public void setOperation(OperationEnum operation) {
        this.operation = operation;
    }
}
