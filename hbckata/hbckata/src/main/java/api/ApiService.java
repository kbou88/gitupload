package api;

import api.dto.Dto;
import api.handler.HandlerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hbckata")
public class ApiService {

    HandlerService handlerService;

    @Autowired
    public ApiService(HandlerService handlerService){
        this.handlerService = handlerService;
    }
    @RequestMapping(value = "/operation/{accountId}",method = RequestMethod.PUT)
    public ResponseEntity operation(Long accountId,@RequestBody Dto dto){
        return  handlerService.operation(accountId,dto);
    }


    @RequestMapping(value = "/operation/{accountId}",method = RequestMethod.GET)
    public ResponseEntity getOperations(Long accountId){
        return  handlerService.getOperations(accountId);
    }



}
