package repository;

import model.Account;
import org.springframework.data.repository.CrudRepository;

public abstract class AccountRepository implements CrudRepository<Account,Long> {
}
