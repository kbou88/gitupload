package common;

import model.Account;
import model.BankingOperation;

public enum OperationEnum {
    DEPOSIT{
        @Override
        public void exec(Account account,BankingOperation bankingOperation) {
            account.deposit(bankingOperation);
        }
    },
    WITHDRAW{
        @Override
        public void exec(Account account,BankingOperation bankingOperation) {
            account.withDrawal(bankingOperation);
        }
    };

    public abstract void exec(Account account,BankingOperation bankingOperation);
}

