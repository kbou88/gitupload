package common.exception;

public class KataException extends Exception{
    public KataException(String message) {
        super(message);
    }
}
