import common.OperationEnum;
import model.Account;
import model.BankingOperation;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;


public class AccountTest {

    @Test
    public void should_return_initial_balance(){
        Account account = new Account();

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(account.getBalance()).isEqualTo(new BigDecimal(0));

        softAssertions.assertAll();

    }

    @Test
    public void should_deposit_amount(){
        Account account = new Account();
        account.deposit(computeBankingOperation(new BigDecimal(1000),OperationEnum.DEPOSIT,account));

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(account.getBalance()).isEqualTo(new BigDecimal(1000));

        softAssertions.assertAll();
    }


    @Test
    public void should_withdraw_amount(){
        Account account = new Account();
        account.withDrawal(computeBankingOperation(new BigDecimal(1000),OperationEnum.DEPOSIT,account));

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(account.getBalance()).isEqualTo(new BigDecimal(-1000));

        softAssertions.assertAll();
    }
    @Test
    public void should_withdraw_and_deposit_amount(){
        Account account = new Account();

        account.withDrawal(computeBankingOperation(new BigDecimal(1000),OperationEnum.DEPOSIT,account));

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(account.getBalance()).isEqualTo(new BigDecimal(-1000));
        softAssertions.assertAll();

        account.deposit(computeBankingOperation(new BigDecimal(2000),OperationEnum.DEPOSIT,account));
        softAssertions.assertThat(account.getBalance()).isEqualTo(new BigDecimal(1000));


        softAssertions.assertAll();
    }

    private BankingOperation computeBankingOperation(BigDecimal amount, OperationEnum operation, Account account) {
        BankingOperation.BankingOperationBuilder bankingOperationBuilder = new BankingOperation.BankingOperationBuilder();
        bankingOperationBuilder.withAmount(amount).withBalance(account.getBalance()).withOperation(operation).withDate(new Date());
        return bankingOperationBuilder.build();
    }
}
