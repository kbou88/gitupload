import common.OperationEnum;
import model.Account;
import model.BankingOperation;
import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Date;


public class BankingOperationTest {

    @Test
    public void should_return_initial_balance(){
        BankingOperation bankingOperation = computeBankingOperation(new BigDecimal(1000),OperationEnum.DEPOSIT,new Account());

        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(bankingOperation.getAmount()).isEqualTo(new BigDecimal(1000));
        softAssertions.assertThat(bankingOperation.getBalance()).isEqualTo(new BigDecimal(0));
        softAssertions.assertThat(bankingOperation.getOperation()).isEqualTo(OperationEnum.DEPOSIT);
        softAssertions.assertAll();

    }

    private BankingOperation computeBankingOperation(BigDecimal amount, OperationEnum operation, Account account) {
        BankingOperation.BankingOperationBuilder bankingOperationBuilder = new BankingOperation.BankingOperationBuilder();
        bankingOperationBuilder.withAmount(amount).withBalance(account.getBalance()).withOperation(operation).withDate(new Date());
        return bankingOperationBuilder.build();
    }
}
